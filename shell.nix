with import <nixpkgs> {};
mkShell {
  buildInputs = [
    gcc
    xorg.libXrandr xorg.libXcursor xorg.libXinerama xorg.libXi xorg.libXext xorg.libXxf86vm
    pkg-config libGL
  ];
}
