package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

const PLAYER_WIDTH int = 80

type Player struct {
	Sprite
}

func (player *Player) Move(posX int) {
	player.Position.Move(float32(posX-player.Width/2), player.Y)
}

func NewPlayer() *Player {
	playerImage := ebiten.NewImage(PLAYER_WIDTH, PLAYER_WIDTH/4)
	playerImage.Fill(color.White)

	return &Player{
		Sprite: Sprite{
			image: playerImage,
			Position: Position{
				X:      float32(WINDOW_WIDTH / 2),
				Y:      float32(WINDOW_HEIGHT * 4 / 5),
				Width:  PLAYER_WIDTH,
				Height: 40,
			},
		},
	}
}
