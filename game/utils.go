package game

import "github.com/hajimehoshi/ebiten/v2"

type Position struct {
	X, Y          float32
	Width, Height int
}

func (p *Position) Move(posX, posY float32) {
	p.X = posX
	p.Y = posY

	if p.X < 0 {
		p.X = 0
	} else if p.X > float32(WINDOW_WIDTH-p.Width) {
		p.X = float32(WINDOW_WIDTH - p.Width)
	}

	if p.Y < 0 {
		p.Y = 0
	} else if p.Y > float32(WINDOW_HEIGHT-p.Height) {
		p.Y = float32(WINDOW_HEIGHT - p.Height)
	}
}

type Sprite struct {
	Position
	image *ebiten.Image
}

func (d *Sprite) Draw(targetImage *ebiten.Image) {
	opts := &ebiten.DrawImageOptions{}
	opts.GeoM.Translate(float64(d.X), float64(d.Y))
	targetImage.DrawImage(d.image, opts)
}
