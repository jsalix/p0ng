package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
)

const BALL_SIZE = 20

type Ball struct {
	Sprite
	velX, velY float32
}

func (ball *Ball) SetVelocity(velX, velY float32) {
	ball.velX = velX
	ball.velY = velY
}

func (ball *Ball) Update() {
	ball.Move(ball.X+ball.velX, ball.Y+ball.velY)

	if ball.X <= 0 || ball.X >= float32(WINDOW_WIDTH-ball.Width) {
		ball.SetVelocity(ball.velX*-1, ball.velY)
	}
	if ball.Y <= 0 || ball.Y >= float32(WINDOW_HEIGHT-ball.Height) {
		ball.SetVelocity(ball.velX, ball.velY*-1)
	}
}

func NewBall(posX, posY, velX, velY float32) *Ball {
	ballImage := ebiten.NewImage(BALL_SIZE, BALL_SIZE)
	ballImage.Fill(color.White)

	return &Ball{
		Sprite: Sprite{
			image: ballImage,
			Position: Position{
				X:      posX,
				Y:      posY,
				Width:  BALL_SIZE,
				Height: BALL_SIZE,
			},
		},
		velX: velX,
		velY: velY,
	}
}
