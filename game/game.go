package game

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
)

const WINDOW_WIDTH int = 600
const WINDOW_HEIGHT int = 800

type Game struct {
	player *Player
	ball   *Ball
}

func (game *Game) Update() error {
	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		log.Fatal("Game has quit")
	}

	cursorX, _ := ebiten.CursorPosition()
	game.player.Move(cursorX)

	game.ball.Update()

	return nil
}

func (game *Game) Draw(screen *ebiten.Image) {
	game.player.Draw(screen)
	game.ball.Draw(screen)
}

func (game *Game) Layout(windowWidth, windowHeight int) (screenWidth, screenHeight int) {
	return WINDOW_WIDTH, WINDOW_HEIGHT
}

func NewGame() *Game {
	return &Game{
		player: NewPlayer(),
		ball: NewBall(
			float32(WINDOW_WIDTH/2-BALL_SIZE/2),
			float32(WINDOW_HEIGHT*1/3-BALL_SIZE/2),
			2.2,
			2.6,
		),
	}
}
