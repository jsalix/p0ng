package main

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/jsalix/p0ng/game"
)

func main() {
	g := game.NewGame()

	ebiten.SetWindowSize(game.WINDOW_WIDTH, game.WINDOW_HEIGHT)
	ebiten.SetWindowTitle("p0ng")

	if err := ebiten.RunGame(g); err != nil {
		log.Fatal(err)
	}
}
